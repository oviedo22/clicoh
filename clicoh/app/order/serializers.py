from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer

from .models import Order, OrderDetail

from app.product.models import Product


class OrderDetailModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = [
            'id',
            'cuantity',
            'last_cuantity',
            'product'
        ]


class OrderModelSerializer(serializers.ModelSerializer):

    details = OrderDetailModelSerializer(many=True)

    class Meta:
        model = Order
        fields = "__all__"

    def validate(self, data):

        order_detail = data.get('details')

        product = order_detail[0]['product']
        cuantity = order_detail[0]['cuantity']
        stock = order_detail[0]['product'].stock
        if cuantity <= stock:
            if self.context['request'].method == 'POST':
                stock = stock-cuantity
                Product.objects.filter(id=product.id).update(stock=stock)
            elif self.context['request'].method == 'PUT':

                last_cuantity = order_detail[0]['last_cuantity'] 

                if last_cuantity:
                    stock = stock + last_cuantity
                    stock = stock-cuantity

                Product.objects.filter(id=product.id).update(stock=stock)
        else:
            raise serializers.ValidationError(
                f'No hay suficiente stock del producto {product}, actualmente hay {stock} unidades')
        return data

    def create(self, validated_data):
        order = Order.objects.create()
        product = ''
        for order_detail in validated_data['details']:
            if order_detail['product'].name != product:
                product = order_detail['product'].name
                details = OrderDetail.objects.create(
                    order=order, cuantity=order_detail['cuantity'], last_cuantity=order_detail['cuantity'], product=order_detail['product'])
            else:
                stock = order_detail['product'].stock
                cuantity = order_detail['cuantity']
                if cuantity <= stock:
                    stock = stock-cuantity
                    Product.objects.filter(
                        id=order_detail['product'].id).update(stock=stock)
                    cuantity = details.cuantity + order_detail['cuantity']
                    OrderDetail.objects.filter(order=order, product__name=product).update(
                        cuantity=cuantity, last_cuantity=cuantity)
                else:
                    raise serializers.ValidationError(
                        f'No hay suficiente stock del producto {product}, actualmente hay {stock} unidades')
        return order

    def update(self, instance, validated_data):
        order = instance
        order.__dict__.update(validated_data)
        order.save()

        for order_detail in validated_data['details']:
            old_detail = OrderDetail.objects.filter(order=order)
            old_cuantity = old_detail[0].cuantity
            OrderDetail.objects.filter(order=order).update(
                cuantity=order_detail['cuantity'],last_cuantity=old_cuantity, product=order_detail['product'])
        return order
