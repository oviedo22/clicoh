from tkinter import CASCADE
from django.db import models
from app.product.models import Product
import requests


# Create your models here.


class Order(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)

    def get_total(self):
        order_details = OrderDetail.objects.filter(order=self.id)
        total=0
        for details in order_details:
            total += details.cuantity * details.product.price
        return total

    def get_total_usd(self):
        order_details = OrderDetail.objects.filter(order=self.id)
        total=0
        for details in order_details:
            total += details.cuantity * details.product.price
        data = requests.get(f'https://www.dolarsi.com/api/api.php?type=valoresprincipales' )
        data= data.json()
        dolar_oficial = float(data[1]['casa']['compra'].replace(',','.') )
        total = total / dolar_oficial
        return total

    def __str__(self):
        return f'{self.id}'


class OrderDetail(models.Model):
    order = models.ForeignKey(
        Order, related_name='details', on_delete=models.CASCADE)
    cuantity = models.IntegerField()
    last_cuantity = models.IntegerField(blank=True, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.order.id} - {self.cuantity} - {self.product}'
