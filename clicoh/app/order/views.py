

from rest_framework import viewsets

from rest_framework.response import Response
from rest_framework.decorators import action
from django.http import Http404

from .models import Order, OrderDetail

from .serializers import OrderDetailModelSerializer, OrderModelSerializer

from app.product.models import Product

# Create your views here.


class OrderDetailViewSet(viewsets.ModelViewSet):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailModelSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderModelSerializer

    @action(methods=['get'], detail=True)
    def get_total(self,request,*args, **kwargs):
        order=self.get_object()
        try:
            return Response(order.get_total())
        except order.DoesNotExist:
            raise Http404

    @action(methods=['get'], detail=True)
    def get_total_usd(self,request,*args, **kwargs):
        order=self.get_object()
        try:
            return Response(order.get_total_usd())
        except order.DoesNotExist:
            raise Http404    

    def destroy(self, request, *args, **kwargs):
       order_details = OrderDetail.objects.filter(order=self.get_object()) 
       for details in order_details:
           cuantity = details.cuantity
           stock = details.product.stock
           product = Product.objects.filter(id = details.product.id).update(stock=cuantity+stock)

       return super(OrderViewSet, self).destroy(request, *args, **kwargs)

