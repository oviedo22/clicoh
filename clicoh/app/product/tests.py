# Django
from django.test import TestCase

# Python
import json

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status

# Models
from .models import Product

class ProductTestCase(TestCase):

    def test_create_product(self):

        client = APIClient()

        test_product = {
            'name': 'Oreo',
            'price': 150,
            'stock': 50,
        }

        response = client.post(
            '/products/', 
            test_product,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('id', result)
        self.assertIn('name', result)
        self.assertIn('price', result)
        self.assertIn('stock', result)

        if 'id' in result:
            del result['id']

        self.assertEqual(result, test_product)


    def test_update_product(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        prod = Product.objects.create(
            name='Oreo',
            price=150,
            stock=20,
        )

        test_product_update = {
            'name': 'Oreo Galleta',
            'price': 155,
            'stock': 25,
        }

        response = client.put(
            f'/products/{prod.id}/', 
            test_product_update,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        if 'id' in result:
            del result['id']

        self.assertEqual(result, test_product_update)

    
    def test_delete_product(self):

        client = APIClient()

        # Creamos un objeto en la base de datos para trabajar con datos
        prod = Product.objects.create(
            name='Oreo',
            price=150,
            stock=20,
        )

        response = client.delete(
            f'/products/{prod.id}/', 
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        prod_exists = Product.objects.filter(id=prod.id)
        self.assertFalse(prod_exists)


    def test_get_product(self):

        client = APIClient()

        Product.objects.create(
            name='Oreo',
            price=150,
            stock=20,
        )

        Product.objects.create(
            name='Pepitos',
            price=140,
            stock=30,
        )

        response = client.get('/products/')
        
        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(result), 2)

        for prod in result:
            self.assertIn('id', prod)
            self.assertIn('name', prod)
            self.assertIn('price', prod)
            self.assertIn('stock', prod)
            break
